<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 15.04.19
 * Time: 9:42
 */

namespace Forpost\Novaclients\utils;


class Json
{
    const JSON_THROW_ON_ERROR = 4194304;

    public static function decode(string $jsonString)
    {
        try {
            return json_decode($jsonString, $assoc = TRUE, $depth = 512, self::JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            print_r($e->getMessage());
        }
    }
}
