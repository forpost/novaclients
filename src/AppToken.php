<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 12.04.19
 * Time: 11:05
 */

namespace Forpost\Novaclients;


use Forpost\Novaclients\transport\HttpTransport;
use Forpost\Novaclients\transport\ITransport;
use Forpost\Novaclients\transport\Transport;
use Forpost\Novaclients\utils\Collection;

class AppToken
{
    const GRANT_TYPE = 'client_credentials';
    const ENDPOINT = '/auth/token';

    private $accessToken;
    private $expires;
    private $client;

    public function __construct()
    {
        $this->transport = (new Transport())->createTransport('Http');
        $this->fetchToken();
    }

    public function fetchToken()
    {
        $token = $this->transport->post(getenv('AUTH_TOKEN_URL'))
            ->body('client_id', getenv('CLIENT_ID'))
            ->body('client_secret', getenv('CLIENT_SECRET'))
            ->body('grant_type', self::GRANT_TYPE)
            ->header('Content-Type',  'application/x-www-form-urlencoded')
            ->exec();
        
        if(!array_key_exists('access_token', $token)) throw new \Exception('BAD_CREDENTIALS');
        $this->setAccessToken($token['access_token']);
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param mixed $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return mixed
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @param mixed $expires
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }


}
