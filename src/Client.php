<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 12.04.19
 * Time: 11:05
 */

namespace Forpost\Novaclients;


use Forpost\Novaclients\transport\Transport;

class Client
{
    private $appToken;
    private $transport;

    public function __construct()
    {
        $appToken = new AppToken();
        $this->appToken = $appToken->getAccessToken();
        $this->transport = (new Transport())->createTransport('Http');
    }

    public function phoneVerify(string $phone)
    {
        return $this->transport->post(getenv('PHONE_VERIFY_URL'))
            ->body('phone', $phone)
            ->auth($this->appToken)
            ->exec();
    }

    public function phoneConfirm($phone, $code)
    {
        return $this->transport->post(getenv('AUTH_TOKEN_URL'))
            ->body('phone', $phone)
            ->body('code', $code)
            ->body('grant_type', 'phone_code')
            ->appendAppCreds()
            ->header('Content-Type', 'application/x-www-form-urlencoded')
            ->exec();
    }

    public function show(string $token, string $id)
    {
        return $this->transport->get(getenv('CLIENTS_URL') . "/" . $id)
            ->auth($token)
            ->exec();
    }

    public function list(string $token, array $query)
    {
        return $this->transport->get(getenv('CLIENTS_URL'))
            ->body($query)
            ->auth($token)
            ->exec();
    }

    public function create(string $token, array $data)
    {
        return $this->transport->post(getenv('CLIENTS_URL'))
            ->body($data)
            ->appendAppCreds()
            ->auth($token)
            ->exec();
    }

    public function me(string $token)
    {
        return $this->transport->get(getenv('ME_URL'))
            ->auth($token)
            ->exec();
    }

    public function verify(string $token, array $data)
    {
        return $this->transport->post(getenv('VERIFY_URL'))
            ->body($data)
            ->auth($token)
            ->exec();
    }
}
