<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 12.04.19
 * Time: 12:10
 */

namespace Forpost\Novaclients\transport;


interface ITransport
{
    public function get(string $url);

    public function post(string $url);
}
