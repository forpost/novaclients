<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 12.04.19
 * Time: 12:09
 */
namespace Forpost\Novaclients\transport;

use Forpost\Novaclients\transport\HttpTransport;

abstract class FactoryAbstract
{
    public function createTransport($type = 'Http'): ITransport {
        switch ($type) {
            case 'Http':
                return new HttpTransport();
            default:
                return new HttpTransport();
        }
    }
}
