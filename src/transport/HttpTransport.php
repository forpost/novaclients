<?php
/**
 * Created by PhpStorm.
 * User: viktor
 * Date: 11.04.19
 * Time: 16:53
 */

namespace Forpost\Novaclients\transport;


use Forpost\Novaclients\utils\Json;
use Forpost\Novaclients\utils\Collection;

class HttpTransport implements ITransport
{

    /**
     * @var resource|false a cURL handle on success, false on errors.
     */
    private $handle = false;
    /**
     * @var Collection
     */
    private $body;
    /**
     * @var Collection
     */
    private $header;

    /**
     * @return Collection
     */
    public function getHeader()
    {
        $iterator = $this->header->getIterator();
        $prepareHeader = new Collection();
        while ($iterator->valid()) {
            $prepareHeader->append("{$iterator->key()}: {$iterator->current()}");
            $iterator->next();
        }
        return $prepareHeader;
    }

    /**
     * @param string $url
     * @param array $headers
     * @return $this
     */
    public function get(string $url): ITransport
    {
        $this->handle = curl_init($url);
        return $this;
    }

    /**
     * @param string $url
     * @param array $data
     * @param array $headers
     * @return $this
     */
    public function post(string $url): ITransport
    {
        $this->handle = curl_init($url);
        curl_setopt($this->handle, CURLOPT_POST, TRUE);
        return $this;
    }

    /**
     * @param string|array $key
     * @param ?string $value
     * @return ITransport $this
     */
    public function body($key, ?string $value = null): ITransport
    {
        $this->body = $this->body ?: new Collection();

        if (is_array($key)) {
            $this->bodyArray($key);
        } else {
            $this->body->offsetSet($key, $value);
        }

        return $this;
    }

    /**
     * @param array $body
     */
    public function bodyArray(array $body)
    {
        foreach ($body as $key => $value) {
            $this->body->offsetSet($key, $value);
        }
    }

    /**
     * @return $this
     */
    public function appendAppCreds()
    {
        $this->body([
            'client_id' => getenv('CLIENT_ID'),
            'client_secret' => getenv('CLIENT_SECRET')
        ]);
        return $this;
    }

    /**
     * @param $value
     * @return $this
     */
    public function header(string $key, string $value): ITransport
    {
        $this->header = $this->header ?:
            new Collection(['Content-Type' => 'application/json'], Collection::STD_PROP_LIST);
        $this->header->offsetSet($key, $value);
        return $this;
    }

    /**
     * @param $token
     * @return $this
     */
    public function auth(string $token): ITransport
    {
        $this->header = $this->header ?: new Collection();
        $this->header->offsetSet("Authorization", "Bearer {$token}");
        return $this;
    }

    /**
     * @return mixed
     */
    public function exec(): array
    {
        if ($this->body) {
            $fields = http_build_query($this->body);
            curl_setopt($this->handle, CURLOPT_POSTFIELDS, $fields);
        }

        curl_setopt($this->handle, CURLOPT_HTTPHEADER, $this->getHeader());

        curl_setopt($this->handle, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($this->handle);
        curl_close($this->handle);
        return Json::decode($result);
    }
}
